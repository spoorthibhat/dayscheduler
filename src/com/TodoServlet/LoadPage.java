package com.TodoServlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.database.DynamoDBClient;
import com.google.gson.Gson;
import com.model.TodoItem;

/**
 * Servlet implementation class LoadPage
 */
@WebServlet("/LoadPage")
public class LoadPage extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoadPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
        String todaysDate = DATE_FORMAT.format(new Date());
        
        DynamoDBClient client = new DynamoDBClient();
        List<TodoItem> itemList= new ArrayList<TodoItem>();
        itemList = client.readTodoItems(todaysDate);
        String todoItem = new Gson().toJson(itemList);
        
        PrintWriter out = response.getWriter();
        out.write(todoItem);
        System.out.println(todaysDate);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	

    

}
