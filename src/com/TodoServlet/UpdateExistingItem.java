package com.TodoServlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.database.DynamoDBClient;

/**
 * Servlet implementation class UpdateExistingItem
 */
@WebServlet("/UpdateExistingItem")
public class UpdateExistingItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateExistingItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
        
		String prevDate = request.getParameter("prevDate");
		String prevTodo = request.getParameter("prevTodo");
		String prevStatus = request.getParameter("prevStatus");
		String prevOwner = request.getParameter("prevOwner");
		String newDate = request.getParameter("date");
		String newTodo = request.getParameter("todo");
		String newStatus = request.getParameter("status");
		String newOwner = request.getParameter("owner");
		
		DynamoDBClient dynamoDBClient = new DynamoDBClient();
		if(prevDate.equals(newDate) && prevTodo.equals(newTodo)) {
			if(!prevStatus.equals(newStatus)) {
				String value = new AddItemTodo().isCompleteStatus(newStatus);
			    dynamoDBClient.updateExistingItem(value, "isComplete", prevDate, prevTodo);
			}
			if(!prevOwner.equals(newOwner)) {
				
			    dynamoDBClient.updateExistingItem(newOwner, "Owner", prevDate, prevTodo);
			}
		}else {
			dynamoDBClient.deleteItem(prevDate, prevTodo);
			String dbStatus = new AddItemTodo().isCompleteStatus(newStatus);
			dynamoDBClient.addItem(dbStatus, newTodo, newDate, newOwner);
		}
		
		response.sendRedirect("/LoadPage");
	}

}
