package com.TodoServlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.database.DynamoDBClient;

/**
 * Servlet implementation class AddItemTodo
 */
@WebServlet("/AddItemTodo")
public class AddItemTodo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String YES = "yes";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddItemTodo() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String isCompleteStatus(String inputStatus){
    	if (inputStatus == "on"){
    		return YES;
    	}else{
    		return "No";
    	}
    }
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String isComplete = isCompleteStatus(request.getParameter("status"));
		String todoItem = request.getParameter("todoPoint");
		String date = request.getParameter("date");
		String owner = request.getParameter("owner");
		
		//Adding to database
		DynamoDBClient dynamoDBClient = new DynamoDBClient();
		dynamoDBClient.addItem(isCompleteStatus(isComplete), todoItem, date, owner);
		
		response.sendRedirect("/LoadPage");
		
	}

}
