package com.TodoServlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class StartPage
 */
@WebServlet("/StartPage")
public class StartPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String CHECKED = "checked";
	private static final String YES = "Yes";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StartPage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"><html><head>");
		out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\"><title>Daily Scheduler</title></head>");
		out.println("<style>table, th , td  {border: 1px solid grey;border-collapse: collapse;padding: 5px;}table tr:nth-child(odd) {background-color: #f1f1f1;}table tr:nth-child(even) {background-color: #ffffff;}</style>");
		out.println("<script src=\"https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js\"></script><body>");
		out.println("<div ng-app = \"scheduler\" ng-controller = \"pageController\"><table>");
		out.println("<tr><td>isComplete</td><td>Todo Task</td><td>Task Date</td><td>Task Owner</td></tr>");
		out.println("<tr ng-repeat = \"x in tasks\"><td>{{x.isDone}}</td><td>{{x.taskName}}</td><td>{{x.date}}</td><td>{{x.owner}}</td></tr></table></div>");
	    out.println("<script>var app = angular.module('scheduler', []);app.controller('pageController', function($scope, $http) {$http({method : \"GET\",url : \"http://localhost:8081/Scheduler/LoadPage\"}).then(function(response) {$scope.tasks = response.data;});});</script>");
	    out.println("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	private String checkBoxSetting(String inputStatus){
    	if (inputStatus == YES){
    		return CHECKED;
    	}else{
    		return " ";
    	}
    }

}
