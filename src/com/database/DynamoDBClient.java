package com.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.model.TodoItem;

public class DynamoDBClient {
	static AWSCredentials credentials; 
	static AmazonDynamoDB dynamoDBClient ; 
	private static final String tablename ="TodoTasksTable" ; 
	static DynamoDB dynamoDB ;
	private static final String CHECKED = "checked";
	private static final String YES = "Yes";
	
	private Map<String, String> expressionAttributeNames;
	
	public DynamoDBClient() {
		credentials = new BasicAWSCredentials("AKIAIMUVMZU3LPT3J5VA", "qKMkByxkDsfAaFJTCVfIstyJYgabZMJEaDT1HE9k");
		dynamoDBClient = AmazonDynamoDBClientBuilder.standard()
							.withRegion("us-east-2")
							.withCredentials(new AWSStaticCredentialsProvider(credentials))
							.build();
		//tablename = "TestTodo";
		dynamoDB = new DynamoDB(dynamoDBClient);
		
		expressionAttributeNames = new HashMap<String, String>();
		expressionAttributeNames.put( "isComplete", "#iC");
		expressionAttributeNames.put( "TodoTask", "#T");
		expressionAttributeNames.put( "TaskDate", "#D");
		expressionAttributeNames.put( "TaskOwner" , "#O");
	}

	public void addItem(String isComplete, String todoItem, String date,String personInCharge) {
		// date is primary key and and todo tem is the sort key
		Table todotable = dynamoDB.getTable(tablename);
		
		Item toBeAdded = new Item().withPrimaryKey("TaskDate" , date ,"TodoTask", todoItem )
									.withString("isComplete",isComplete)
									.withString("TaskOwner", personInCharge);
		todotable.putItem(toBeAdded);
	}
	
	public void updateExistingItem(String value, String fieldToBeUpdated , String primaryKey , String todoValue){
		
		Table todotable = dynamoDB.getTable(tablename);
		
		// String columnField = fieldToBeUpdated; // *TODO* : Get the fieldToBeUpdated correctly(Example: It should give like "Date" or "Todo Item", etc
		Map<String,Object> expressionAttributeValues = new HashMap<String, Object>();
		expressionAttributeValues.put(":val", value);
		//expressionAttributeValues.put(":todoVal", todoValue);

		/*UpdateItemOutcome outcome = todotable.updateItem(
				new PrimaryKey("Date" , primaryKey , "Todo Item" ,todoValue) ,
				"set " + columnField + " = :val", // UpdateExpression
				
			    expressionAttributeNames,
			    expressionAttributeValues);*/
		UpdateItemSpec spec = new UpdateItemSpec().withPrimaryKey("TaskDate", primaryKey , "TodoTask", todoValue)
												  .withUpdateExpression("set " +fieldToBeUpdated+  "= :val")
												  .withValueMap(expressionAttributeValues);
		todotable.updateItem(spec);
	}
	
	public List<TodoItem> readTodoItems(String date){
		
		Table todotable = dynamoDB.getTable(tablename);

        HashMap<String, Object> valueMap = new HashMap<String, Object>();
        valueMap.put(":date", date);

        QuerySpec querySpec = new QuerySpec().withKeyConditionExpression("TaskDate = :date").withValueMap(valueMap);
        										//.withNameMap(expressionAttributeNames)
        										

        ItemCollection<QueryOutcome> items = null;
        items = todotable.query(querySpec);
        return translateFromDynamoDB(items);
	}
	
	public void deleteItem(String primKey, String todoTask ){
		Table todotable = dynamoDB.getTable(tablename);
		
		Map<String,Object> expressionAttributeValues = new HashMap<String,Object>();
		expressionAttributeValues.put(":val", todoTask);

		try{	
			DeleteItemSpec itemSpec = new DeleteItemSpec().withPrimaryKey("TaskDate", primKey , "TodoTask", todoTask);
			todotable.deleteItem(itemSpec);
		} catch (Exception e) {
			//TODO - Read about logging, log4j and use log4j instead of doing Syster.err.
            System.err.println("Unable to delete item ");
            System.err.println(e.getMessage());
        }
	}
	
	public TodoItem getSingleItem(String date, String task){
		Table todotable = dynamoDB.getTable(tablename);
		
		GetItemSpec itemSpec = new GetItemSpec().withPrimaryKey("TaskDate", date , "TodoTask", task);
		Item toBeReturned = todotable.getItem(itemSpec);
		// System.out.println(toBeReturned.toJSONPretty());
		return translateFromDynamoDB(toBeReturned);
		
	}
	
	private TodoItem translateFromDynamoDB(Item item) {
		
		TodoItem outputItem = new TodoItem();
		outputItem.setIsDone(item.getString("isComplete"));
		outputItem.setTaskName(item.getString("TodoTask"));
		outputItem.setDate(item.getString("TaskDate"));
		outputItem.setOwner(item.getString("TaskOwner"));
		return outputItem;
	}
	
	private List<TodoItem> translateFromDynamoDB(ItemCollection<QueryOutcome>  items) {
		
		Iterator<Item> iterator = null;
        Item item = null;
		iterator = items.iterator(); 
		
		List<TodoItem> todoList = new ArrayList<TodoItem>();
        while (iterator.hasNext()) {
           
          item = iterator.next();
          TodoItem outputItem = new TodoItem();
          outputItem.setIsDone(item.getString("isComplete"));
          outputItem.setTaskName(item.getString("TodoTask"));
          outputItem.setDate(item.getString("TaskDate"));
          outputItem.setOwner(item.getString("TaskOwner"));
          todoList.add(outputItem);
          
		  }
        return todoList;
	}
	
    
    
	public static void main(String[] args) {
		
		DynamoDBClient obj = new DynamoDBClient();
		/*obj.addItem("yes", "Meet X", "01/25/2018", "Spoo");
		obj.addItem("no", "Meet Ana", "01/25/2018", "Spoo");*/
		//obj.addItem("Yes", "Do coding", "01/30/2018", "Akshay");
		//obj.addItem("No", "Test the loadPage logic", "02/08/2018", "Spoorthi");
		//obj.addItem("No", "Add the logic for front end using AngualrJS", "02/08/2018", "Spoorthi");
		
		// obj.updateExistingItem("no", "isComplete", "01/30/2018", "Do coding");
		
		// obj.deleteItem("01/25/2018", "Meet X");
		// obj.getSingleItem("01/30/2018", "Do coding");
		List<TodoItem> testList= new ArrayList<TodoItem>();
		testList = obj.readTodoItems("02/08/2018");
		
		for(TodoItem i : testList){
			System.out.println(i.getTaskName());
		}
	}
	
}
